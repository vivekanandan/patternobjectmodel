package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods {
	
	
	public ViewLeadsPage() {
		
		//page factory
		
		PageFactory.initElements(driver, this);
		
	}
    @FindBy ( how = How.ID,using ="viewLead_firstName_sp") WebElement eleViewFirstName;
    
	public ViewLeadsPage VerifyFirstName()
	{
		
		String FirstName = getElementText(eleViewFirstName);
		
		System.out.println(FirstName);
		
		boolean verifyExactText = verifyExactText(eleViewFirstName, "vivek");
		System.out.println(verifyExactText);
		return this;
		
	}
	
}
