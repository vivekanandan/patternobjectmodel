package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	
	public CreateLeadPage() {
		
		//page factory
		
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy (how = How.ID,using ="createLeadForm_companyName") WebElement eleCMName;
	@FindBy (how = How.ID,using ="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy (how = How.ID,using ="createLeadForm_lastName") WebElement eleLastName;
	@FindBy (how = How.XPATH,using="//input[@name='submitButton']") WebElement eleCreateLeadClick;
	
	public CreateLeadPage EnterCompanyName(String data) {
		
		clearAndType(eleCMName, data);
		return this;
		
	}
	
    public CreateLeadPage EnterFirstName(String data) {
		
		clearAndType(eleFirstName, data);
		return this;
		
	}
	
    public CreateLeadPage EnterLastName(String data) {
		
		clearAndType(eleLastName, data);
		return this;
		
	}
    
   public ViewLeadsPage ClickCreateLeadButton() {
		
		click(eleCreateLeadClick);
		
		return new ViewLeadsPage();
		
	}
    
	
}
