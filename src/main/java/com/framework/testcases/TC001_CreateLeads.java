package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLeads extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_CreateLeads";
		testDescription="Create Leads";
		testNodes="Leads";
		author="Vivek";
		category="smoke";
		dataSheetName="TC001";
				
	}
	
	
	@Test(dataProvider="fetchData")
	public void login(String username,String password,String companyname,String Firstname,String Lastname)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.ClickCRMSFA()
		.ClickLeads()
		.ClickCreateLead()
		.EnterCompanyName(companyname)
		.EnterFirstName(Firstname)
		.EnterLastName(Lastname)
		.ClickCreateLeadButton()
		.VerifyFirstName(); 
		
		
		
	}
	
}
